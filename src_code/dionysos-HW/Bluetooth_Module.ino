
void sendBluetoothData(String str){

  blueToothSerial.print(str + "\r");
}

void readBluetoothData(){

  bool messageStart = false;
  String message;
  String timeLeftStr;
  int stop = 0;
  
  while(blueToothSerial.available()) {

    delay(10);  // small delay_ms to allow buffer to fill
    char c = blueToothSerial.read();

    if(message == "$3:") {

      if(c == '$') {
        
        timeLeft = timeLeftStr.toInt();
        break;
      }
      timeLeftStr += c;
      continue;
    }

    message += c;
    
    if(c == '$') {

      stop++;
    }

    if(stop == 2) {

      break;
    }
  }

  if(message == "$4:0$") {

    btMessage = NOT_WARNING;
  }
  
  if(message == "$4:1$") {

    btMessage = WARNING;
  }
  
  switch(btMessage) {

    case WARNING: {

      updateEventLCD("WARNING!");
      sendBluetoothData("$4:1$");
      btMessage = NO_MESSAGE;
      tone(BUZZER, 1000, 5000);
      break;
    }

    case NOT_WARNING: {

      clearRow(ROW.upper);
    }
  }
}
