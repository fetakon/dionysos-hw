#ifndef _CONSTANTS_H
#define _CONSTANTS_H

typedef struct {

  int lower= 1;
  int upper = 0;
}Row;

typedef enum {WARNING, NOT_WARNING, NO_MESSAGE} MessageState;
typedef enum {BREAK_ROOM, CONTROL_ROOM, REACTOR_ROOM} RoomState;

#define SS_PIN 10
#define RST_PIN 9
#define RX_PIN 3
#define TX_PIN 2
#define RADIATION_INPUT_PIN A0
#define RADIATION_INPUT_FACTOR 10.23
#define LEFT 0
#define CENTER 1
#define RIGHT 2
#define HAZMATSUIT A1
#define ROOM_PIN A2
#define BUZZER A3

const Row ROW;

#endif
