
int convertRadiationInput(int input) {

  return (input > 10) ? ceil(input / RADIATION_INPUT_FACTOR) : floor(input / RADIATION_INPUT_FACTOR); 
}

void updateTimer(){

  if(clockTimer>=1000 && isLoggedIn){
    
    if(timeLeft>=0){
      
      formatIncomingTime(timeLeft, hour, minute, second);
      printTimeLeft(hour, minute, second);
      
      clockTimer=0;
      timeLeft--;
    }
  }
}

ISR(TIMER0_COMPA_vect){    //This is the interrupt request

  if(!blipOK) {

    blipTimer++;
    if(blipTimer >= 2000) {

      blipOK = true;
      blipTimer = 0;
    }
  }
  clockTimer++;

  if(isLoggedIn) {
    if(radiationMessageTimer == 2000) {

    int radiationToSend = convertRadiationInput(lastInput);
    sendBluetoothData("$1:" + String(radiationToSend) + "$");
    radiationMessageTimer ++;
    
    } else if(radiationMessageTimer >= 10000) {

    radiationMessageTimer = 2001; // to prevent inifity
    
    } else {

    radiationMessageTimer++;
    }
  }

  if(hazmatTimer >= 500) {

    isHazmatOk = true;
  } else if(hazmatTimer > 10000) {

    hazmatTimer = 501;
  }
  hazmatTimer++;

  if(roomTimer >= 500) {

    isRoomOk = true;
  } else if(roomTimer > 10000) {

    roomTimer = 501;
  } 
  roomTimer++;
}
