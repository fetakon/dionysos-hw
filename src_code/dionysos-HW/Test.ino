void setupTest() {
  // put your setup code here, to run once:
  
  //testConvertRadiationInput();
  //testFormatIncomingTime();
  //testPrintTimeLeft();
  //testPutMessageMiddle();
  testWarning();
  
}

void testForClock1(bool clockInterupt){
  
  if(clockInterupt){
    Serial.print("Test succeded"); 
  }
  else{
    Serial.print("Test Failed");
  }
}

void testConvertRadiationInput() {

// ARRANGE
  int sut1 = 1023;
  int sut2 = 0;
  int sut3 = 511;
  
// ACT
  displayRadiation(convertRadiationInput(sut3));

/*  ASSERT
 *  CHECK DISPLAY
 */

}

void testPrintTimeLeft() {

  //ARRANGE
  long hour = 4;
  long minute = 50;
  long second = 47;

  //ACT
  printTimeLeft(hour, minute, second);

/*  ASSERT
 *  CHECK DISPLAY
 */
 
}

void testFormatIncomingTime() {

  //ARRANGE
  long sut1 = 3600;
  long sut2 = 60000;
  long sut3 = 0;
  long hour,minute,second;

  //ACT
  formatIncomingTime(sut1, hour, minute, second);
  printTimeLeft(hour, minute, second);

/*  ASSERT
 *  CHECK DISPLAY
 */
  
}

void testPutMessageMiddle(){

  //ARRANGE
  String sut1 = "HELLO";
  String sut2 = "ARDUINO";
  String sut3 = "DIS";

  //ACT
  lcd.setCursor(putMessageMiddle(sut1), 0);
  lcd.print(sut1);

/*  ASSERT
 *  CHECK DISPLAY
 */ 
}

void testWarning() {

  //ARRANGE

  //ACT
  btMessage = WARNING;

/*  ASSERT
 *  CHECK DISPLAY
 */ 
}
