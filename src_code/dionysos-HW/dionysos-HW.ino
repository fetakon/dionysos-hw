
#include <LiquidCrystal.h>
#include <SPI.h>
#include <MFRC522.h>
#include <SoftwareSerial.h>
#include "CONSTANTS.h"

// VARIABLES //

MFRC522 mfrc522;//(SS_PIN, RST_PIN);   // Create MFRC522 instance.
SoftwareSerial blueToothSerial(TX_PIN, RX_PIN);
LiquidCrystal lcd(0, 1, 4, 5, 6, 7);

int blipTimer = 0, clockTimer=0, lastInput = 0;
int radiationMessageTimer = 2001;    // Tracking when to send 
int hazmatTimer = 0;
int roomTimer = 0;
int lcdBlinkTimer = 0;
int hazmatButtonState = 0;
int roomButtonState = 0;
int column = 0;

long hour, minute, second;
long timeLeft;

bool blipOK = true;
bool isLoggedIn = false;
bool hazmatSuitToggle = false;
bool isHazmatOk = true;
bool isRoomOk = true;

String content= "";

MessageState btMessage = NO_MESSAGE;
RoomState nextRoom = BREAK_ROOM;

void setup() 
{
  TCCR0A=(1<<WGM01);    //Set the CTC mode   
  OCR0A=0xF9; //Value for ORC0A for 1ms 
  
  TIMSK0|=(1<<OCIE0A);   //Set the interrupt request
  sei(); //Enable interrupt
  
  TCCR0B|=(1<<CS01);    //Set the prescale 1/64 clock
  TCCR0B|=(1<<CS00);
  
  SPI.begin();      // Initiate  SPI bus
  mfrc522.PCD_Init();   // Initiate MFRC522
  blueToothSerial.begin(9600);
  lcd.begin(16,2);
  pinMode(RADIATION_INPUT_PIN, INPUT);  // Initiate Potentiometer pin for radiation

  pinMode(BUZZER, OUTPUT);
  pinMode(HAZMATSUIT, INPUT);
  pinMode(ROOM_PIN, INPUT);
  //setupTest();
  lastInput = analogRead(RADIATION_INPUT_PIN);
  displayRadiation(convertRadiationInput(lastInput));
  
}
void loop() {
  
  readHazmatSuitStatus();
  observeRoomStatus();
  observeRadiationInput();
  readBluetoothData();

  updateTimer();
  
  // Look for new cards
  if ( ! mfrc522.PICC_IsNewCardPresent()) {
    
    return;
  }
  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial()) {
    
    return;
  }
  
  createRFIDtoString();
  observeBlip();
  
}

// FUNCTIONS //
void formatIncomingTime( long timeLeft, long &hours, long &minutes, long &seconds ){
  
  long t = timeLeft;
  
  seconds = t % 60;
  
  t = (t - seconds)/60; 
  minutes = t % 60;
  
  t = (t - minutes)/60;
  hours = t;
}

void observeRadiationInput() {

  int input; 
  input = analogRead(RADIATION_INPUT_PIN);

  if(input > (lastInput + RADIATION_INPUT_FACTOR) || input < (lastInput - RADIATION_INPUT_FACTOR)) {

    lastInput = input;
    radiationMessageTimer = 0;
    displayRadiation(convertRadiationInput(lastInput));
  }
}

void readHazmatSuitStatus(void){

  hazmatButtonState = digitalRead(HAZMATSUIT);
  if(hazmatButtonState == LOW && isHazmatOk && isLoggedIn){
    hazmatSuitToggle = !hazmatSuitToggle;
    if(hazmatSuitToggle == true && isLoggedIn){

      updateEventLCD("SUIT ON");
      sendBluetoothData("$2:1$");  
    }
    else{

      updateEventLCD("SUIT OFF");
      sendBluetoothData("$2:0$");
    }
    hazmatTimer = 0;
    isHazmatOk = false;
  }
}

void updateRoomStatus(){

  switch(nextRoom){

    case BREAK_ROOM:{

      sendBluetoothData("$5:1$");
      updateEventLCD("IN BREAK ROOM");
      nextRoom = REACTOR_ROOM;
      break;
    }

    case REACTOR_ROOM:{

      sendBluetoothData("$5:2$");
      updateEventLCD("IN REACTOR ROOM");
      nextRoom = CONTROL_ROOM;
      break;
    }

    case CONTROL_ROOM:{

      sendBluetoothData("$5:3$");
      updateEventLCD("IN CONTROL ROOM");
      nextRoom = BREAK_ROOM;
      break;
    }
  }
}

void observeRoomStatus(){

  roomButtonState = digitalRead(ROOM_PIN);
  if(roomButtonState == LOW && isRoomOk && isLoggedIn) {

    updateRoomStatus();
    roomTimer = 0;
    isRoomOk = false;
  }
}

void createRFIDtoString(){
  
  content= "";
  for (byte i = 0; i < mfrc522.uid.size; i++) {
    
     content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
     content.concat(String(mfrc522.uid.uidByte[i], HEX));
  }
}

void observeBlip(){

  content.toUpperCase();
  if (content.substring(1) == "26 04 25 F8"){ //change here the UID of the card/cards that you want to give access
    
    if(isLoggedIn && blipOK){
      
      isLoggedIn = false;
      blipOK = false;
      sendBluetoothData("$0:0$");
      updateEventLCD("CHECK OUT");
      tone(BUZZER, 300, 300);
      
    }
    else if(!isLoggedIn && blipOK){

      blipOK = false;
      isLoggedIn = true;
      sendBluetoothData("$0:1$");
      int radiationToSend = convertRadiationInput(lastInput);
      sendBluetoothData("$1:" + String(radiationToSend) + "$");
      sendBluetoothData("$5:1$");   //Sends the initial value "BREAK_ROOM"
      nextRoom = REACTOR_ROOM;
      updateEventLCD("CHECK IN");
      tone(BUZZER, 1000, 300);
    }
  }
}
