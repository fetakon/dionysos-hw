#include "CONSTANTS.h"

void printTimeLeft(long &hour, long &minute, long &second) {

  String timeToShow;

  if(hour < 10) {

    timeToShow += "0" + String(hour) + ":";
  } else {

    timeToShow += String(hour) + ":";
  }

  if(minute < 10) {
    
    timeToShow += "0" + String(minute) + ":";
  } else {

    timeToShow += String(minute) + ":";
  }

  if(second < 10) {

    timeToShow += "0" + String(second);
  } else {

    timeToShow += String(second);
  }

  if(hour < 100){

    lcd.setCursor(0, ROW.lower);
    lcd.print(timeToShow + " ");
    
  } else if(hour >= 1000) {

    lcd.setCursor(0, ROW.lower);
    lcd.print("> 1000h  ");
    
  } else{

    lcd.setCursor(0, ROW.lower);
    lcd.print(timeToShow);
  }
}

void updateEventLCD(String message) {

  clearRow(ROW.upper);
  lcd.setCursor(putMessageMiddle(message), ROW.upper);
  lcd.print(message);
}

int putMessageMiddle(String message) {

  int messageLength = message.length();
  int col = (16 - messageLength) / 2;
  return col;
}

void clearRow(int row){

  for(int i = 0; i<16; i++){
    
    lcd.setCursor(i,row);
    lcd.print(" ");
  }
}

void displayRadiation(int radiation) {

  lcd.setCursor(9,ROW.lower);
  lcd.print("RAD: ");
  
  if(radiation < 10) {

    lcd.setCursor(13,ROW.lower);
    lcd.print(radiation);
    lcd.setCursor(14,ROW.lower);
    lcd.print(" ");
    lcd.setCursor(15,ROW.lower);
    lcd.print(" "); 
  }

  else if(radiation > 99) {

    lcd.setCursor(13,ROW.lower);
    lcd.print("100");
  }

  else {

    lcd.setCursor(13,ROW.lower);
    lcd.print(radiation);
    lcd.setCursor(15,ROW.lower);
    lcd.print(" ");
  }
}
