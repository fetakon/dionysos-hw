
int counter = 0;
int timer=0;
void setup() {
    
  TCCR0A=(1<<WGM01);    //Set the CTC mode   
  OCR0A=0xF9; //Value for ORC0A for 1ms 
  
  TIMSK0|=(1<<OCIE0A);   //Set the interrupt request
  sei(); //Enable interrupt
  
  TCCR0B|=(1<<CS01);    //Set the prescale 1/64 clock
  TCCR0B|=(1<<CS00);

 Serial.begin(9600);
    
}

void loop() {
  //in this way you can count 1 second because the nterrupt request is each 1ms
  if(timer>=1000){
    timer=0;
    counter++;
    Serial.print(counter%60);
  }
  
  
  
}

ISR(TIMER0_COMPA_vect){    //This is the interrupt request
  timer++;
}
